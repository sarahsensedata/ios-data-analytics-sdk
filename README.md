# 1st Screen - iOS Data Analytics SDK

###

Documentation and files for 1st Screen clients that are integrating the aOS Data Analytics SDK

This SDK provides an API set that allows apps owners to collects the full breadth of user app data for the purpose of commercialisation through advertising and remarketing


---


## SDK Build

### Current Version 1.0.0


Add the source in your `pod` file   

```ruby
platform :ios, '9.0'

source 'https://webfitnz@bitbucket.org/webfitnz/1stscreencocoapods.git'
source 'https://github.com/CocoaPods/Specs.git'
```   

then add the dependency for this framework

`pod 'FSAnalytics'`   


### Dependencies:


The SDK has a minimal list of dependancies that we update during our quarterly release cycle. We always aim to reduce the footprint of our technology

*


---


## Usage


#### Initialization


Your `UIApplicationDelegate` must be a subclass of `FSAppDelegate`. Initialize the FirstScreen SDK right after the application has launched

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // set the block that initializes the SDK
        analyticsInitializer = {
            FSAnalyticsTracker.initialize(isSandboxMode: true, apiKey: API_WRITE_KEY, encryptionKey: Util.getKey() as Data)
        }

        // initialize the tracker
        initializeFSSDK()

        return true
}
```

`FSAnalyticsTracker` requires a `Configuration` for setup. The method `FSAnalyticsTracker.initialize(isSandboxMode: Bool, apiKey: String, encryptionKey: Data?)` looks for a configuration JSON file in the app's
root folder. The default configuration file names for Sandbox and Production mode are respectively `fs_config_sandbox.json`
and `fs_config.json`.   

###


_**Mandatory** paramters to include in the configuration file:_

_Key_                         |_Type_     |_Explanation_
----------------------------|---------|-----------------------------------------------------------------
isSandboxMode               | boolean | Is in sandbox/debug mode
publisherId                 | String  | Publisher/Partner ID
buid                        | String  | App Bundle ID.

_**Optional** paramters to include in the configuration file:_

_Key_                         |_Type_     |_Explanation_
----------------------------|---------|-----------------------------------------------------------------
dataCollectionOff           | boolean | If true then only anonymous SDK log data is collected
reasonForNotTracking        | String  | Select from - coppa / consent / gdpr / 
apparea                     | String  | App Section Name
siid                        | String  | App Section ID
kews                        | String  | App Page Keywords
uage                        | String  | Users Age
usge                        | String  | Users Gender
pode                        | String  | Users Post Code.

###

These optional parameters have been standardised, by 1st Screen, based on our client activity to date. This is not an exhaustive list and any custom parameter can be added. Please ensure to communicate any additional parameters during the integration process to ensure full commercialisation of the data
   
If you want your config file to be named or stored differently than the default then you can provide the url of that file while initializng the tracker.

```swift
FSAnalyticsTracker.initialize(apiKey: String, encryptionKey: Data?, configFileUrl: URL)
```



### Manual Initialization:


You can also build a `Configuration` manually in code and then use that to initialize the tracker.
```swift
let config = Configuration.builder()
                        .withSandboxMode(isSandbox: true)
                        .withApiKey(writeKey: "")
                        .withPublisherId(id: "")
                        .withDataCollectionOff(off: false)
                    .build()
    
FSAnalyticsTracker.initialize(config)
```

   
   
In a `FSAppDelegate` subclass if you override either `func applicationDidBecomeActive(_ application: UIApplication)` or `func applicationWillResignActive(_ application: UIApplication)` then don't forget to call the super method from the overriden one.
   
   
If you are using background app refresh and has overriden `func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)` then after you are done with fetching your data call the super method and let the super implementation invoke the `completionHandler`. If you want your fetch result to be passed to the `completionHandler` then set the `bgFetchResult` before calling super.


### Additional Tracking Options:



Page views are automatically tracked for `ViewController`s that are subclass of `FSViewController`. To provide a custom page name set the `pageName`

```swift
override func viewDidLoad() {
        super.viewDidLoad()

        pageName = "Product-Details"
    }
```

In a `FSViewController` subclass if you override either `func viewDidAppear(_ animated: Bool)` or `func viewWillDisappear(_ animated: Bool)` then don't forget to call super method from the overriden one.

All other relevant page views can be tracked like:
```swift
FSAnalyticsTracker.sharedInstance?.trackPageView(pageName: "Page-Name", callback: {error in
            
        })
```


### Custom Event:



You can track a simple event like this:
```swift
let event  = FSEvent(type: "MY-EVENT", details: "my event occurred")
        FSAnalyticsTracker.sharedInstance?.addEvent(datasetName: nil, tableName: nil, event: event, callback: {error in
            
        })
```

You can provide user information to the SDK like this:
```swift
FSAnalyticsTracker.sharedInstance?.userAge = 30
        FSAnalyticsTracker.sharedInstance?.userGender = "M"
        FSAnalyticsTracker.sharedInstance?.userPostcode = "2022"
        FSAnalyticsTracker.sharedInstance?.userLocationLatitude = -23.112235
        FSAnalyticsTracker.sharedInstance?.userLocationLongitude = 122.998768
```



### Custom Data:


You can add additional data to your event using:
```swift
let eventData: [String:Any] = [:]
        let enhancedEvent = try? FSAnalyticsTracker.sharedInstance?.addAdditionalData(event: eventData)
```



### Error Handling 


`addEvent` methods cache events locally and then the SDK uploads them in batches optimally. If you are using `FSAppDelegate` and `FSViewController` then event uploading is automatically done for you in an optimum way. 

You can try to upload the event right now instead of caching using:
```swift
FSAnalyticsTracker.sharedInstance?.trackEvent(datasetName: nil, tableName: "TABLE", data: eventData, callback: {error  in
            
        })
```
The event will still be cached and uploaded later if the web request fails for some reason.

In case you want to manually upload the cached events, you can do so by:
```swift
FSAnalyticsTracker.sharedInstance?.uploadAllEvents(callback: {error in
            
        })
```


---


##Location Data


if you are tracking user location or your app is aware of the device location then you can set a delegate through which the SDK can then retrieve location data
```swift
extension AppDelegate: LocationProvider {
    var latitude: Double? {
        return currentLocation?.coordinate.latitude
    }

    var longitude: Double? {
        return currentLocation?.coordinate.longitude
    }
}
...

FSAnalyticsTracker.sharedInstance?.locationProvider = UIApplication.shared.delegate as? LocationProvider
```


---


## Deleting Data


You can request for deletion of all data for this specific device/user using:
```swift
FSAnalyticsTracker.sharedInstance?.requestToDeleteData("Reason for deletion", callback: {error in
            
        })
```
This API has been included with the intention of it being built into a button that can be included on the privacy page of the application. This would allow users to request the deletion of their data. All user data collected through use of the apllciation and against the users advertsiing ID will be deleted after this API is activated. Future data will still be collected and its up the the application to restrict data collection using the configuration file.

When using this API we reccomend that a message similar to the below is presented to the user

"Your request has been sent and your historical data will be deleted within a reasonable timeframe".